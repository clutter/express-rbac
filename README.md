# Express RBAC
[Role Based Access Control](https://www.npmjs.com/package/@parthar/rbac) for [Express](https://expressjs.com/)


## Overview
This module provides a RBAC Express middleware for access-control to routes based on @parthar/rbac module.

Assumes a [passport](http://www.passportjs.org/) like interface is used for authentication, and [express-session](https://www.npmjs.com/package/express-session) like sessions:
* req.isAuthenticated() - function to validate user-authentication status
* req.user - user-details JSON-object populated on request
    * user.roles - list/array of roles assigned to user (if any)
    * user.perms - list/array of additional permissions assigned to user (if any)
* req.session - optional user-sesssion populated on request (if available)

The user-session (if available) is used to cache the expanded list of permissions for efficiency/optimization purposes; an "_rbacPerms" key is created on the req.session for this purpose. In this case, the user will have to logout and login again for the updated roles/permissions to reflect.


## Usage

```js
const express = require("express");
const expressRBAC = require("@parthar/express-rbac");

var app = express();
// use store-instance from @parthar/rbac
var rbac = expressRBAC(store);
// middleware to check update-access to user-profile
app.put("/user/profile", rbac("profile", "update"), function (req, res, next) {
    db.updateUserProfile(req.body, function (err) {
        res.sendStatus(err ? 500 : 200);
    });
});
```

## Middleware - expressRBAC(store)(resource, action)
The middleware returns the following statuses:
1. HTTP-401 : req.isAuthenticated() returns false
1. HTTP-403 : user.role/user.perms do not have the necessary permissions
1. HTTP-500 : internal error

Use [debug](https://www.npmjs.com/package/debug) module with "express-rbac" to print additional debug output.
