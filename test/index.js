/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint newline-per-chained-call: "off"*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const RBAC = require("@parthar/rbac");
const Store = RBAC.Store;
const Permissions = RBAC.Permissions;
const Grants = RBAC.Grants;
const expressRBAC = require("../");

var store;
var grants;
var rbac;

before(async () => {
    var perms;

    store = new Store();
    grants = new Grants(store);
    perms = new Permissions().resource("profile").actions(["read", "update"]);
    await grants.role("user").allow(perms).commit();
    perms = new Permissions().resource("profile").actions(["create", "delete"]);
    await grants.role("admin").allow(perms).commit();
    perms = new Permissions().resource("*").actions(["*"]);
    await grants.role("super").allow(perms).commit();
    rbac = expressRBAC(store);
});

describe("express-rbac", function () {
    it("should export express middleware", (done) => {
        assert.strictEqual(typeof expressRBAC, "function");
        assert.throws(() => expressRBAC());
        assert.strictEqual(typeof expressRBAC(store), "function");
        assert.strictEqual(typeof expressRBAC(store)("res", "act"), "function");
        assert.strictEqual(expressRBAC(store)("res", "act").length, 3);
        done();
    });
    it("should send HTTP-500 if req.isAuthenticated is not defined", (done) => {
        rbac("res", "act")({}, {
            "sendStatus": function (status) {
                assert.ok(status === 500);
                done();
            }
        });
    });
    it("should send HTTP-401 if req.isAuthenticated() is false", (done) => {
        rbac("res", "act")({
            "isAuthenticated": function () {
                return false;
            }
        }, {
            "sendStatus": function (status) {
                assert.ok(status === 401);
                done();
            }
        });
    });
    it("should allow for valid resource-actions - user", (done) => {
        rbac("profile", "read")({
            "isAuthenticated": function () {
                return true;
            },
            "user": {
                "roles": ["user"]
            }
        }, {}, function () {
            assert.ok(arguments.length === 0);
            done();
        });
    });
    it("should allow for valid resource-actions - admin", (done) => {
        rbac("profile", "create")({
            "isAuthenticated": function () {
                return true;
            },
            "user": {
                "roles": ["admin"]
            }
        }, {}, function () {
            assert.ok(arguments.length === 0);
            done();
        });
    });
    it("should allow for valid resource-actions - user & admin", (done) => {
        rbac("profile", "delete")({
            "isAuthenticated": function () {
                return true;
            },
            "user": {
                "roles": ["user"],
                "perms": new Permissions().resource("profile").actions(["create", "delete"]).export()
            }
        }, {}, function () {
            assert.ok(arguments.length === 0);
            done();
        });
    });
    it("should allow for any resource-actions - super", (done) => {
        rbac("any", "any")({
            "isAuthenticated": function () {
                return true;
            },
            "user": {
                "roles": ["super"]
            }
        }, {}, function () {
            assert.ok(arguments.length === 0);
            done();
        });
    });
    it("should deny for invalid resource-actions", (done) => {
        rbac("profile", "read")({
            "isAuthenticated": function () {
                return true;
            },
            "user": {
                "roles": ["admin"]
            }
        }, {
            "sendStatus": function (status) {
                assert.ok(status === 403);
                done();
            }
        });
    });
    it("should update session if available", (done) => {
        let req = {
            "isAuthenticated": function () {
                return true;
            },
            "user": {
                "roles": ["admin"]
            },
            "session": {}
        };

        rbac("profile", "delete")(req, {}, () => {
            assert.deepEqual(req.session._rbacPerms,
                new Permissions().resource("profile").actions(["create", "delete"]).export());
            done();
        });
    });
    it("should use session if available", (done) => {
        let req = {
            "isAuthenticated": function () {
                return true;
            },
            "session": {
                "_rbacPerms": new Permissions().resource("profile").actions(["create", "delete"]).export()
            }
        };

        rbac("profile", "delete")(req, {}, () => {
            assert.ok(arguments.length === 0);
            done();
        });
    });
});

after(function (done) {
    done();
});
