/*eslint consistent-return: "off"*/

"use strict";

const each = require("async-each");
const rbac = require("@parthar/rbac");
const debug = require("debug")("express-rbac");
const Store = rbac.Store;
const Permissions = rbac.Permissions;

const INTERNAL_ERROR = 500;
const NOT_AUTHENTICATED = 401;
const NOT_AUTHORIZED = 403;

function isString(value) {
    return typeof value === "string" || value instanceof String;
}

function getPermissions4Roles(store, roles, callback) {
    let perms = [];

    each(roles, function iterator(rol, next) {
        store.getRolePermissions(rol, function result(err, pems) {
            if (!err) {
                perms = perms.concat(pems);
            }
            next(err);
        });
    }, function final(err) {
        callback(err, perms);
    });
}

module.exports = function expressRBAC(store) {
    if (store instanceof Store === false) {
        if (debug.enabled) {
            debug("input-param => store: %s", store instanceof Store === true ? "ok" : "err");
        }
        throw new Error("invalid 'store' parameter");
    }

    return function rbacFunc(resource, action) {
        if (isString(resource) === false || isString(action) === false) {
            if (debug.enabled) {
                debug("input-param => resource: %s", isString(resource) === true ? "ok" : "err");
                debug("input-param => action: %s", isString(action) === true ? "ok" : "err");
            }
            throw new Error("invalid input parameter(s)");
        }


        return function rbacMiddleware(req, res, next) {
            var perms = [];

            function checkAccess() {
                if (req.session && !req.session._rbacPerms) { // cache for future requests
                    debug("caching permissions for future requests");
                    req.session._rbacPerms = perms;
                }
                if (new Permissions(perms).can(resource, action)) {
                    next();
                } else {
                    res.sendStatus(NOT_AUTHORIZED);
                }
            }

            if (!req.isAuthenticated) {
                debug("req.isAuthenticated is not defined");

                return res.sendStatus(INTERNAL_ERROR); // no passport?
            }
            if (!req.isAuthenticated()) {
                debug("req.isAuthenticated() is false");

                return res.sendStatus(NOT_AUTHENTICATED); // not authenticated
            }
            if (req.session && req.session._rbacPerms) {
                perms = req.session._rbacPerms;
                checkAccess();
            } else {
                if (req.user.perms) {
                    perms = perms.concat(req.user.perms);
                }
                if (req.user.roles) {
                    getPermissions4Roles(store, req.user.roles, function rolePerms(err, pems) {
                        if (err) {
                            debug("store.getPermissions4Roles() returned error: %o", err);

                            return res.sendStatus(INTERNAL_ERROR);
                        }
                        perms = perms.concat(pems);
                        checkAccess();
                    });
                } else {
                    checkAccess();
                }
            }
        };
    };
};
